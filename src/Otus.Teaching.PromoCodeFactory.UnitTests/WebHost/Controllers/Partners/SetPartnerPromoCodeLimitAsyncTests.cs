﻿using System.Threading.Tasks;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IDisposable
    {
        
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnersRepositoryMock = new Mock<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
        }

        //Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Return_BadRequest_If_Partner_Is_NotFound()
        {
            //Arrange
            SetPartnerPromoCodeLimitRequest request = GetSetPartnerPromoCodeLimitRequest(); // fabric init

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(System.Guid.Empty, request);

            //Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        //Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Return_BadRequest_If_Partner_Is_NotActive()
        {
            //Arrange
            SetPartnerPromoCodeLimitRequest request = GetSetPartnerPromoCodeLimitRequest(); //fabric init

            Partner partner = new PartnerBuilder()
                                    .WithIsActive(false)
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(System.Guid.Empty, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();

            var badRequestResult = result as BadRequestObjectResult;
            badRequestResult.Value.ToString().Should().Be("Данный партнер не активен");
        }

        //Лимит должен быть больше 0;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Return_BadRequest_If_Limit_Is_NotPositive()
        {
            //Arrange
            SetPartnerPromoCodeLimitRequest request = GetSetPartnerPromoCodeLimitRequest(); //fabric init

            Partner partner = new PartnerBuilder()
                                    .WithIsActive(true)
                                    .WithPartnerLimits(new List<PartnerPromoCodeLimit>())
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(System.Guid.Empty, request);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();

            var badRequestResult = result as BadRequestObjectResult;
            badRequestResult.Value.ToString().Should().Be("Лимит должен быть больше 0");
        }
        
        //При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Cancel_Previous_Limit_If_LimitIsSet()
        {
            //Arrange
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequestBuilder()
                                                            .WithLimit(10)
                                                            .Build();

            DateTime cancelDate = DateTime.Now.AddDays(-1);
            PartnerPromoCodeLimit partnerPromoCodeLimit = new PartnerPromoCodeLimitBuilder()
                                                            .WithId(Guid.NewGuid())
                                                            .WithCancelDate(cancelDate)
                                                            .Build();

            Partner partner = new PartnerBuilder()
                                    .WithId(Guid.NewGuid())
                                    .WithIsActive(true)
                                    .WithPartnerLimits(new List<PartnerPromoCodeLimit>() { partnerPromoCodeLimit })
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeOfType<CreatedAtActionResult>();
            partner.PartnerLimits.First(x => x.Id == partnerPromoCodeLimit.Id).CancelDate.Should().NotBe(DateTime.Now.AddDays(-1));

        }
        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Reset_NumberOfPromocodes_If_LimitIsSet()
        {
            //Arrange
            DateTime endDate = DateTime.Now.AddDays(1);
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequestBuilder()
                                                            .WithLimit(10)
                                                            .WithEndDate(endDate)
                                                            .Build();

            
            PartnerPromoCodeLimit partnerPromoCodeLimit = new PartnerPromoCodeLimitBuilder()
                                                            .WithId(Guid.NewGuid())
                                                            .Build();

            Partner partner = new PartnerBuilder()
                                    .WithId(Guid.NewGuid())
                                    .WithIsActive(true)
                                    .WithPartnerLimits(new List<PartnerPromoCodeLimit>() { partnerPromoCodeLimit })
                                    .WithNumberIssuedPromoCodes(55)
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeOfType<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }

        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Should_Not_Reset_NumberOfPromocodes_If_LimitIsOver()
        {
            //Arrange
            DateTime endDate = DateTime.Now.AddDays(-1);
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequestBuilder()
                                                            .WithLimit(10)
                                                            .WithEndDate(endDate)
                                                            .Build();


            PartnerPromoCodeLimit partnerPromoCodeLimit = new PartnerPromoCodeLimitBuilder()
                                                            .WithId(Guid.NewGuid())
                                                            .Build();

            Partner partner = new PartnerBuilder()
                                    .WithId(Guid.NewGuid())
                                    .WithIsActive(true)
                                    .WithPartnerLimits(new List<PartnerPromoCodeLimit>() { partnerPromoCodeLimit })
                                    .WithNumberIssuedPromoCodes(55)
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeOfType<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(55);

        }
        //Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Verify_Id_NewLimit_Was_Saved_To_Database()
        {
            //Arrange
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequestBuilder()
                                                            .WithLimit(10)
                                                            .Build();

            DateTime cancelDate = DateTime.Now.AddDays(-1);
            PartnerPromoCodeLimit partnerPromoCodeLimit = new PartnerPromoCodeLimitBuilder()
                                                            .WithId(Guid.NewGuid())
                                                            .WithCancelDate(cancelDate)
                                                            .Build();

            Partner partner = new PartnerBuilder()
                                    .WithId(Guid.NewGuid())
                                    .WithIsActive(true)
                                    .WithPartnerLimits(new List<PartnerPromoCodeLimit>() { partnerPromoCodeLimit })
                                    .Build();

            //Act
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            _partnersRepositoryMock.Verify(m => m.UpdateAsync(partner), Times.Once);
            partner.PartnerLimits.Count.Should().BeGreaterThan(1);
            result.Should().BeOfType<CreatedAtActionResult>();

        }



        private SetPartnerPromoCodeLimitRequest GetSetPartnerPromoCodeLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0,
                EndDate = DateTime.Now
            };
        }

        private PartnerPromoCodeLimit GetPartnerPromoCodeLimit()
        {
            return new PartnerPromoCodeLimit
            {
                Id = Guid.NewGuid(),
            };

        }

        public void Dispose()
        {
            
        }
    }
}