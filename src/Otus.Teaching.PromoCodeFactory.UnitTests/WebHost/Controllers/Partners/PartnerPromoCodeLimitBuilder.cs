﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitBuilder
    {
        private Guid _id;
        private DateTime _cancelDate;

        public PartnerPromoCodeLimitBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithCancelDate(DateTime cancelDate)
        {
            _cancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit()
            {
                Id = _id,
                CancelDate = _cancelDate
            };
        }
    }
}
